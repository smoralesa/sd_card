/**
 * spi_32.h
 * diegomedina
 */

#ifndef spi32__h
#define spi32__h

// SPI functions.
// Private function prototypes
volatile uint32_t time_var1, time_var2;

void Delay(volatile uint32_t nCount);
void SPI_write(uint16_t);
uint8_t SPI_read();

#endif
