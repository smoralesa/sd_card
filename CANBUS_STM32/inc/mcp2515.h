/**
 * mcp2515.h
 * diegomedina
 */

#ifndef mcp2515__h
#define mcp2515__h

// MCP2515 functions.
typedef struct
{
	uint16_t id;
	struct {
		int8_t rtr : 1;
		uint8_t length : 4;
	} header;
	uint8_t data[8];
} tCAN;

// ----------------------------------------------------------------------------
uint8_t spi_putc( uint8_t data );

// ----------------------------------------------------------------------------
void mcp2515_write_register( uint8_t adress, uint8_t data );

// ----------------------------------------------------------------------------
uint8_t mcp2515_read_register(uint8_t adress);

// ----------------------------------------------------------------------------
void mcp2515_bit_modify(uint8_t adress, uint8_t mask, uint8_t data);

// ----------------------------------------------------------------------------
uint8_t mcp2515_read_status(uint8_t type);

// ----------------------------------------------------------------------------

uint8_t mcp2515_init(uint8_t speed);

// ----------------------------------------------------------------------------
// check if there are any new messages waiting
uint8_t mcp2515_check_message(void);

// ----------------------------------------------------------------------------
// check if there is a free buffer to send messages
uint8_t mcp2515_check_free_buffer(void);

// ----------------------------------------------------------------------------
uint8_t mcp2515_get_message(tCAN *message);

// ----------------------------------------------------------------------------
uint8_t mcp2515_send_message(tCAN *message);

#endif
