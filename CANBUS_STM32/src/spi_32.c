/*
 * canbus.c
 *
 *  Created on: 28 sep 2016
 *      Author: diegomedina
 */
 #include <stdio.h>
//#include <stdlib.h>
//#include <stdint.h>
#include <math.h>
#include "stm32f4xx_conf.h"
#include "utils.h"
#include "canbus.h"
#include "global.h"
#include "mcp2515.h"
#include "mcp2515_defs.h"
#include "spi_32.h"

#include <stdint.h>
#include <stdio.h>

void SPI_write(uint16_t data){
  int spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_TXE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to send data over SPI bus\n");
     }
  }
  SPI_I2S_SendData(SPI1,data);
  spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_RXNE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to receive data over SPI bus\n");
     }
  }
  SPI_I2S_ReceiveData( SPI1 );
}

uint8_t SPI_read(){
  int spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_TXE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to send data over SPI bus\n");
     }
  }
  SPI_I2S_SendData(SPI1,0xFF);
  spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_RXNE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to receive data over SPI bus\n");
     }
  }
  return (uint8_t) SPI_I2S_ReceiveData( SPI1 );
}

