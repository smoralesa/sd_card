#include <stdio.h>
//#include <stdlib.h>
//#include <stdint.h>
#include <math.h>
#include "stm32f4xx_conf.h"
#include "utils.h"

// Private variables
volatile uint32_t time_var1, time_var2;
char pri[10] = {'1','2','3','4',0};
char pas = 0;

// Private function prototypes
void Delay(volatile uint32_t nCount);
void init();
void calculation_test();
void SPI_NSS_high();
void SPI_NSS_low();
void SPI_write(uint16_t);
uint8_t SPI_read();

int main(void) {
	init();

	calculation_test();


	return 0;
}


void calculation_test() {

	int iteration = 0;
	int spi_retry;
	uint16_t spi_rx1 = 4;
  uint16_t spi_rx2 = 4;
  uint16_t spi_rx3 = 4;
  uint16_t spi_rx4 = 4;
  uint16_t spi_rx5 = 4;
	Delay(2000);
	int i = 0;
	for (i = 0; i <10; i++){
    spi_retry = 0;
    while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_TXE ) )
    {
       if ( 0xFF == spi_retry++ )
       {
          printf("Reached SPI hardware retries trying to send data over SPI bus\n");
       }
    }
    SPI_I2S_SendData(SPI1,0xFF);
	}
	Delay(1);
	SPI_NSS_low();
	Delay(1);
	SPI_write(0x40);
  SPI_write(0x00);
  SPI_write(0x00);
  SPI_write(0x00);
  SPI_write(0x00);
  SPI_write(0x95);
  Delay(1);
  spi_rx1 = SPI_read();
  spi_rx2 = SPI_read();
  spi_rx3 = SPI_read();
  spi_rx4 = SPI_read();
  spi_rx5 = SPI_read();

	SPI_NSS_high();

	printf("El spi received es: %i\n", spi_rx1);
  printf("El spi received es: %i\n", spi_rx2);
  printf("El spi received es: %i\n", spi_rx3);
  printf("El spi received es: %i\n", spi_rx4);
  printf("El spi received es: %i\n", spi_rx5);
	for(;;) {
		GPIO_SetBits(GPIOD, GPIO_Pin_12);
		Delay(500);
		GPIO_ResetBits(GPIOD, GPIO_Pin_12);
		Delay(500);

		printf("Iteration: %i\n", iteration);

		iteration++;
	}
}

void init() {
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;

	// ---------- SysTick timer -------- //
	if (SysTick_Config(SystemCoreClock / 1000)) {
		// Capture error
		while (1){};
	}

	// GPIOD Periph clock enable
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	// Configure PD12, PD13, PD14 and PD15 in output pushpull mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);


	// ------ UART6 ------ //
	//RX = PC6
	//TX = PC7
	// Clock
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6);
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

	// Conf
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init(USART6, &USART_InitStructure);

	// Enable
	USART_Cmd(USART6, ENABLE);

	// ---------- SPI ---------- //

	// Clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// Configuration
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master; //Debe ser master claramente
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;//8 bits el byte
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; //Segun leí esto sería aproximadamente 350k
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_Init(SPI1, &SPI_InitStructure);

  // IO
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5 | GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //Será que esto es así?
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_SetBits(GPIOA, GPIO_Pin_4);

	SPI_Cmd(SPI1,ENABLE);
}

void SPI_NSS_high(){
  GPIO_SetBits(GPIOA, GPIO_Pin_4);
}

void SPI_NSS_low(){
  GPIO_ResetBits(GPIOA, GPIO_Pin_4);
}

/*
 * Called from systick handler
 */
void timing_handler() {
	if (time_var1) {
		time_var1--;
	}

	time_var2++;
}

/*
 * Delay a number of systick cycles (1ms)
 */
void Delay(volatile uint32_t nCount) {
	time_var1 = nCount;
	while(time_var1){};
}

/*
 * Dummy function to avoid compiler error
 */
void _init() {

}

void SPI_write(uint16_t data){
  int spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_TXE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to send data over SPI bus\n");
     }
  }
  SPI_I2S_SendData(SPI1,data);
  spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_RXNE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to receive data over SPI bus\n");
     }
  }
  SPI_I2S_ReceiveData( SPI1 );
}

uint8_t SPI_read(){
  int spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_TXE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to send data over SPI bus\n");
     }
  }
  SPI_I2S_SendData(SPI1,0xFF);
  spi_retry = 0;
  while ( SET != SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_RXNE ) )
  {
     if ( 0xFF == spi_retry++ )
     {
        printf("Reached SPI hardware retries trying to receive data over SPI bus\n");
     }
  }
  return (uint8_t) SPI_I2S_ReceiveData( SPI1 );
}
